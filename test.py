def my_func():
    a = 23
    b = 42

    this_is_a_very_long_variable_name = "This is a very long string that will definitely exceed the preferred line length limit of 80 characters"

    print(this_is_a_very_long_variable_name)
    print(a)
    print(b)
