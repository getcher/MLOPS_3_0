# Contributing to MLOPS_3_0

We welcome contributions to our project! This document provides guidelines and requirements to help you make your contribution.

## Working with Code

### Using Linters

We use several linters to maintain the quality and consistency of our codebase. It's important that your code adheres to the standards set by these tools.

#### Ruff

[Ruff](https://github.com/charliermarsh/ruff) is a fast linter for Python that helps detect various issues in the code, from stylistic discrepancies to potential errors.

##### Installation

Ensure you have `ruff` installed:

```bash
pip install ruff
```

##### Running the Linter

You can run ruff locally on your machine to check your code before committing:

```bash
ruff path/to/your/code/
```

If ruff identifies any issues, it will list them in the console. It's recommended to fix all warnings and errors to maintain high code quality.

Integration with Pre-commit
We use pre-commit to automatically check the code before each commit. To set it up, follow these steps:

Install pre-commit:

```bash
pip install pre-commit
```

Set up the pre-commit configuration in your local repository:

```bash
pre-commit install
```

Now, pre-commit will automatically run ruff (and other configured tools) before each commit.

By following these guidelines, you help us maintain and improve the quality of our project. Thank you for your contribution!
